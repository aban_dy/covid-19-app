import React, { useEffect,useState } from 'react';
import Header from './components/Header';
import Body from './components/Body';

function App() {


  /*
    States
  */
  const [ image, setImage ] = useState(null); // Image
  const [ isFormOpen, setIsFormOpen ] = useState(false); // trigger form
  const [ safetyMeasures, setSafetyMeasures ] = useState([
          { id : 1, title : "Drink Water", body : "drink water every 20 minutes" }
    ]);

  const [ isEditing, setIsEditing ] = useState(false);
  const [ itemToEdit, setItemToEdit ] = useState({});



  /*
    Fetch
  */
  useEffect(()=>{

    fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/random_masks_usage_instructions.php", {
      "method": "GET",
      "headers": {
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "dcde9fa080msh4c8e6753c4f5be2p1d8d4fjsnde34a78eba98"
      }
    })
    .then(response => {
      return response.blob();
    })
    .then(blob=>{
      setImage(URL.createObjectURL(blob));
      console.log(blob);
    });

    

  },[])

  /*
    Functions
  */

  const toggleForm = (sm) => {

    setIsFormOpen(!isFormOpen);
    setIsEditing(false);
    setItemToEdit(sm);


  }

  const deleteOne = id => {
    let newMeasures = safetyMeasures.filter(sm=>{
      return sm.id !== id
    })

    setSafetyMeasures(newMeasures);
  }

  const saveOne = (title,body)=>{

    if(isEditing){
      safetyMeasures.map(sm=>{
        if(itemToEdit.id === sm.id){
          title === "" ? sm.title = itemToEdit.title : sm.title = title;
          body === "" ? sm.body = itemToEdit.body : sm.body = body;
        }
      });
      setIsEditing(false);
      setItemToEdit({});
    }else{
      let id = safetyMeasures.length;
       let newMeasure = {
        id : id + 1,
        title : title,
        body : body
      }
      let newMeasures = [newMeasure,...safetyMeasures];
       setSafetyMeasures(newMeasures);
    }

  }

  
  console.log(isEditing);

  return (
    <React.Fragment>
      <div className="container p-3">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 my-2">
            <Header image = { image }/>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 my-2">
            <Body 
              isFormOpen = { isFormOpen }
              toggleForm = { toggleForm }
              safetyMeasures = { safetyMeasures }
              deleteOne = { deleteOne }
              saveOne = { saveOne }
              isEditing = { isEditing }
              itemToEdit = { itemToEdit }
              setIsEditing = { setIsEditing }
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default App;

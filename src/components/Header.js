import React from 'react';

const Header = props => {
	return(
			<React.Fragment>
				<div>
					<img src={ props.image } className="img-fluid" alt="" />
				</div>
			</React.Fragment>
		)
}

export default Header;
import React from 'react';
import Form from './Form';
import SafetyMeasure from './SafetyMeasure';

const Body = ({ isFormOpen, toggleForm, safetyMeasures, deleteOne, saveOne, saveEdit, isEditing, itemToEdit,setIsEditing}) => {
	return(
		<React.Fragment>
			<div className="row">
				<div className="col-lg-12">
					<div className="bg-primary p-5 rounded text-center">
						<button className="btn btn-primary"
							onClick = { toggleForm }
						>
							<span>+&nbsp;</span>
							add preventive measures
						</button>
					</div>
				</div>
			</div>

			<div className="row">
				<div className="col-lg-12">
					{ safetyMeasures.map(sm=>(
							<SafetyMeasure
								key = { sm.id }
								id={ sm.id }
								title={ sm.title }
								body={ sm.body }
								deleteOne = { deleteOne }
								toggleForm = { toggleForm }
								sm = { sm }
								setIsEditing={ setIsEditing }
							/>
					)) }
				</div>
			</div>
			<Form 
				isFormOpen = { isFormOpen }
				toggleForm = { toggleForm }
				saveOne = { saveOne }
				saveEdit = { saveEdit }
				isEditing = { isEditing }
				safetyMeasures = { safetyMeasures }
				itemToEdit = { itemToEdit }
				setIsEditing = { setIsEditing }
			/>
		</React.Fragment>
	)
}

export default Body;
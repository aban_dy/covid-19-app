import React  from 'react';
import { Card,CardHeader,CardBody,Button,CardFooter } from 'reactstrap';



const SafetyMeasure = ({toggleForm,title,body,deleteOne,id,sm,setIsEditing}) => {
	return(
		<React.Fragment>
			<Card className="my-3 text-dark"> 
				<CardHeader>
					<strong className="text-center">{ title }</strong>
				</CardHeader>
				<CardBody>
					<p>{ body }</p>
				</CardBody>
				<CardFooter>
					<Button 
						color="danger"
						onClick={ ()=>deleteOne(id) }
					>Delete</Button>
					{ ' ' }
					<Button 
						color="info"
						onClick={()=>{
							toggleForm(sm);
							setIsEditing(true);
						}}
						>Edit</Button>
				</CardFooter>
			</Card>
		</React.Fragment>
	)
}

export default SafetyMeasure;
import React,{ useState } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	FormGroup,
	Label,
	Input,
	Button,
} from 'reactstrap';

const Form = ({ isFormOpen, toggleForm, saveOne, saveEdit, isEditing, itemToEdit,setIsEditing}) => {

	const [title, setTitle] = useState("");
	const [body, setBody] = useState("");

	return(
			<React.Fragment>
				<Modal
					isOpen = { isFormOpen }
					toggle = { toggleForm }
				>
					<ModalHeader
						toggle = { toggleForm }
					>
					{isEditing === true ? 'Edit preventive measures' :'Add preventive measures'}
					</ModalHeader>
					<ModalBody>
						<FormGroup>
							<Label>Title</Label>
							<Input 
								placeholder= "title"
								defaultValue= { isEditing ? itemToEdit?.title : title }
								onChange={ (e)=>setTitle(e.target.value) }
							/>
						</FormGroup>
						<FormGroup>
							<Label>Description</Label>
							<Input 
								placeholder="description"
								defaultValue={ isEditing ? itemToEdit?.body : body}
								onChange={ (e)=>setBody(e.target.value) }
							/>
						</FormGroup>
						<Button
							color="success"
							onClick={()=>{
								saveOne(title,body);
								toggleForm();
								setIsEditing(true);
								setBody("");
								setTitle("");
							}}

						>{ isEditing ? "Edit" : "Add" }</Button>
					</ModalBody>
				</Modal>
			</React.Fragment>
		)
}

export default Form;